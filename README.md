This is a CloudLab or Emulab (https://www.cloudlab.us,
https://www.emulab.net) profile that mocks up the wireless-backhaul'd
POWDER aggregates, and their connection to Emulab central.
