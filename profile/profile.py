#!/usr/bin/env python2

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the InstaGENI library.
import geni.rspec.igext as ig
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

from lxml import etree as ET

class IPAssign(object):

    def __init__(self,baseaddr=[10,0,0,0],basebits=8,
                 minsubnetbits=None,maxsubnetbits=None,
                 allowed_subnets=None,ipAllocationUsesCIDR=False):
        self.baseaddr = baseaddr
        self.basebits = basebits
        if minsubnetbits:
            if minsubnetbits < 2:
                raise Exception("minimum subnet mask size is 2")
            if minsubnetbits < basebits:
                raise Exception(
                    "minsubnetbits (%d) must be greater than basebits (%d)"
                    % (self.minsubnetbits,self.basebits))
            self.minsubnetbits = minsubnetbits
        else:
            minsubnetbits = basebits + 1
        if maxsubnetbits:
            if maxsubnetbits >= 30:
                raise Exception("maximum subnet mask size is 30")
            self.maxsubnetbits = maxsubnetbits
        else:
            self.maxsubnetbits = 32 - 4
        if self.basebits >= self.maxsubnetbits:
            raise Exception(
                "basebits (%d) must be less than maxsubnetbits (%d)"
                % (self.basebits,self.maxsubnetbits))
        self.networks = dict()
        self.netmasks = dict()
        self.lastaddr = dict()
        # The list of allowed subnet bit sizes.
        if allowed_subnets:
            self.allowed_subnets = allowed_subnets
        elif ipAllocationUsesCIDR:
            subbits = basebits + 1
            self.allowed_subnets = []
            while subbits <= maxsubnetbits:
                self.allowed_subnets.append(subbits)
                subbits += 1
        else:
            self.allowed_subnets = [ 16,24 ]
        self.allowed_subnets.sort()
        # An ordered (increasing bit size) list of (bitsize,networkname) tuples.
        self.required_subnets = []
        # A dict of key networkname and value {
        #   'bits':bitsize,'network':networkint,
        #   'hostnum':hostint,'maxhosts':maxhostsint,'netmaskstr':'x.x.x.x' }
        self.subnets = {}

    def __repr__(self):
        return "IPAssign(%s/%s)" % (
            ".".join(map(lambda x: str(x),self.baseaddr)),self.basebits)

    @staticmethod
    def __htoa(h):
        return "%d.%d.%d.%d" % (
            (h >> 24) & 0xff,(h >> 16) & 0xff,(h >> 8) & 0xff,h & 0xff)

    def request_network(self,name,numhosts):
        #
        # Here, we just reserve each network's bitspace, in increasing
        # bitsize.  This makes eventual assignment trivial; we just pick the
        # next hole out of the previous (larger or equiv size) subnet.
        #
        _sz = numhosts + 1
        i = 0
        while _sz > 0:
            i += 1
            _sz = _sz >> 1
        # The minimum network size is a 255.255.255.252 (a /30).
        if i < 2:
            i = 2
        # Change the host bitsize to a network bitsize.
        i = 32 - i
        sn = None
        for _sn in self.allowed_subnets:
            if i < _sn:
                break
            sn = _sn
        if sn == None:
            raise Exception("network of bitsize %d (%d hosts) does not fit into"
                            " network with basebits %d" % (i,numhosts,basebits))
        ipos = 0
        for (bitsize,_name) in self.required_subnets:
            if sn < bitsize:
                break
            ipos += 1
        self.required_subnets.insert(ipos,(sn,name))
        return

    def assign_networks(self):
        basenum = self.baseaddr[0] << 24 | self.baseaddr[1] << 16 \
            | self.baseaddr[2] << 8 | self.baseaddr[3]
        # If any of our network broadcast addrs exceed this address, we've
        # overallocated.
        nextbasenum = basenum + 2 ** (32 - self.basebits)

        #
        # Process self.required_subnets list into self.subnets dict, and
        # setup the next host number.  Again, we assume that
        # self.required_subnets is ordered in increasing number of
        # network bits required.
        #
        lastbitsize = None
        lastnetnum = None
        for i in range(0,len(self.required_subnets)):
            (bitsize,name) = self.required_subnets[i]
            if lastbitsize != bitsize and lastbitsize != None:
                # Need to get into the next lastbitsize subnet to allocate
                # for the bitsize subnet.
                lastnetnum += 2 ** (32 - lastbitsize)

            if lastnetnum != None:
                netnum = lastnetnum + 2 ** (32 - bitsize)
            else:
                netnum = basenum
            if netnum >= nextbasenum:
                raise Exception("out of subnet space in /%d at network %s (/%d)"
                                % (basebits,name,bitsize))
            bcastnum = netnum + (2 ** (32 - bitsize)) - 1
            netmask = 0
            for i in range(32 - bitsize,32):
                netmask |= (1 << i)
            self.subnets[name] = {
                'bits':bitsize,'netnum':netnum,
                'networknum':netnum,'networkstr':IPAssign.__htoa(netnum),
                'bcastnum':bcastnum,'bcaststr':IPAssign.__htoa(bcastnum),
                'hostnum':0,'maxhosts':2 ** (32 - bitsize) - 1,'addrs':[],
                'netmask':netmask,'netmaskstr':IPAssign.__htoa(netmask)
            }
            lastnetnum = netnum
            lastbitsize = bitsize

    def get_network(self,name):
        return self.subnets[name]['networkstr']

    def get_network_cidr(self,name):
        return "%s/%d" % (self.subnets[name]['networkstr'],
                          self.subnets[name]['bits'])

    def get_network_bits(self,name):
        return self.subnets[name]['bits']

    def get_network_mask(self,name):
        return self.subnets[name]['netmaskstr']

    def assign_host(self,name):
        #
        # Assign an IP address in the given name.  If you call this function
        # more times than you effectively promised (by calling
        # ipassign_add_network with a requested host size), you will be
        # handed an Exception.
        #
        bitsize = None
        for (_bitsize,_name) in self.required_subnets:
            if _name == name:
                bitsize = _bitsize
        if not bitsize:
            raise Exception("unknown network name %s" % (name,))
        if not _name in self.subnets:
            raise Exception("unknown network name %s" % (name,))

        maxhosts = (2 ** bitsize - 1)
        if self.subnets[name]['hostnum'] >= maxhosts:
            raise Exception("no host space left in network '%s' (max hosts %d)"
                            % (name,maxhosts))

        self.subnets[name]['hostnum'] += 1

        addr = IPAssign.__htoa(self.subnets[name]['netnum']
                               + self.subnets[name]['hostnum'])
        if True:
            self.subnets[name]['addrs'].append(addr)
        return (addr,self.subnets[name]['netmaskstr'])

#
# Add some shell script vars as an rspec resource.  Easily parsed out of
# the manifest.
#
class ClientVariables(pg.Resource):
    def __init__(self):
        super(ClientVariables,self).__init__()
        self.vars = {}

    def addVariable(self,var,value):
        if type(value) == int or type(value) == float:
            self.vars[var] = str(value)
        elif type(value) == bool:
            if value:
                self.vars[var] = str(1)
            else:
                self.vars[var] = str(0)
        else:
            value = str(value)
            if value.find("'") >=0:
                self.vars[var] = '"%s"' % (str(value))
            else:
                self.vars[var] = "'%s'" % (str(value))

    def _write(self, root):
        ns = "{http://www.protogeni.net/resources/rspec/ext/johnsond/1}"
        paramXML = "%svariable" % (ns,)
        el = ET.SubElement(root,"%sprofile_variables" % (ns,))
        for k in sorted(self.vars.keys()):
            elc = ET.SubElement(el,paramXML)
            elc.text = '%s=%s' % (k,self.vars[k])
        return el
    pass

clientvars = ClientVariables()

# Create a portal object,
pc = portal.Context()

TOPO_SUBNET = "subnet"
TOPO_NET30 = "net30"
TOPO_NET30MP = "net30mp"

pc.defineParameter(
    "vpnType","VPN Type",portal.ParameterType.STRING,TOPO_NET30MP,
    [(TOPO_SUBNET,TOPO_SUBNET),(TOPO_NET30,TOPO_NET30),
     (TOPO_NET30MP,TOPO_NET30MP)],
    # ("bridged","bridged")
    longDescription="VPN Type.  routed-privaddr means a routed L3 VPN whose link endpoints are constructed using private IP space.  bridged means a bridged L2 VPN.")
pc.defineParameter(
    "numAggregates","Number Aggregates",portal.ParameterType.INTEGER,2,
    longDescription="The number of aggregates.")
pc.defineParameter(
    "aggregatesPerHost","Aggregates per Host",portal.ParameterType.INTEGER,4,
    longDescription="Number of Aggregates per physical host.")
pc.defineParameter(
    "aggregateSubnetBitSize","Aggregate Subnet Bit Size",
    portal.ParameterType.INTEGER,29,
    longDescription="Aggregate subnet bit size.")
pc.defineParameter(
    "aggUplinksAreLANs","Aggregate Uplinks are LANs",
    portal.ParameterType.BOOLEAN,True,
    longDescription="Set true if each aggregate uplink is just a member of a big LAN; false if you want each aggregate to uplink router connection to be its own private link.")
pc.defineParameter(
    "vpnCompression","VPN Compression",portal.ParameterType.STRING,"none",
    [("none","none"),("yes","yes")],
    longDescription="Compression type.")
pc.defineParameter(
    "vpnCipher","VPN Cipher",portal.ParameterType.STRING,"none",
    [("none","none"),("yes","yes"),("AES-128-CBC","AES-128-CBC"),
     ("AES-256-CBC","AES-256-CBC"),("AES-256-GCM","AES-256-GCM")],
    longDescription="Cipher string.  Defaults to AES-128-CBC if 'yes'.")
pc.defineParameter(
    "virtualizeAllFixedNodes","Virtualize all Fixed Nodes",
    portal.ParameterType.BOOLEAN,False,
    longDescription="If this is set, the con, fw, mboss, wifi, and mobile fixed nodes will be virtualized, in addition to the mops and inet fixed nodes.  You only want to select this option if you don't care about performance testing; it isn't a good idea for any nodes on the high-bandwidth paths to be VMs for that, obviously.")
pc.defineParameter(
    "doAllNucs","Add All Compute NUCs to Aggregates",
    portal.ParameterType.BOOLEAN,False,
    longDescription="Defaults to adding only one placeholder compute nuc.")
pc.defineParameter(
    "hostType","VM Host Node Type",portal.ParameterType.NODETYPE,"d430",
    longDescription="Node type of physical VM host nodes.")
pc.defineParameter(
    "coresPerVM","Cores per VM",portal.ParameterType.INTEGER,0,
    longDescription="Number of cores each VM will get.")
pc.defineParameter(
    "ramPerVM","RAM per VM",portal.ParameterType.INTEGER,2048,
    longDescription="Amount of RAM each VM will get.")
pc.defineParameter(
    "vmImage","VM Image",portal.ParameterType.STRING,
    'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD',
    longDescription="The image your VMs will run.")
pc.defineParameter(
    "hostImage","Host Image",portal.ParameterType.STRING,
    'urn:publicid:IDN+emulab.net+image+emulab-ops//XEN49-64-STD',
    longDescription="The image your VM host nodes will run (ensure compat with VM Type choice!)")
pc.defineParameter(
    "multiplex","Multiplex Networks",portal.ParameterType.BOOLEAN,True,
    longDescription="Multiplex all LANs and links.")
pc.defineParameter(
    "bestEffort","Best Effort Link Bandwidth",portal.ParameterType.BOOLEAN,True,
    longDescription="Do not require guaranteed bandwidth throughout switch fabric.")
pc.defineParameter(
    "trivialOk","Trivial Links Ok",portal.ParameterType.BOOLEAN,True,
    longDescription="Maybe use trivial links.")

pc.defineParameter(
    "fixedLinkSpeed","Core High-bandwidth Link Speed",
    portal.ParameterType.INTEGER,0,
    [(0,"Default"),(100000,"100Mb/s"),(1000000,"1Gb/s"),(10000000,"10Gb/s")],
    longDescription="A specific link speed to use for the high-bandwidth paths (mboss<>fw, fw<>con.  Other fixed node links are 1Gbps.  If you force all fixed nodes to be VMs, the default value for this is 1Gbps; else, 10Gbps.")
pc.defineParameter(
    "wifiLinkSpeed","WiFi Uplink Speed",portal.ParameterType.INTEGER,10000000,
    [(0,"Any"),(100000,"100Mb/s"),(1000000,"1Gb/s"),(10000000,"10Gb/s")],
    longDescription="A specific link speed to use for the wifi uplink.  This value defaults to 1Gbps.")
pc.defineParameter(
    "wifiLinkLatency","WiFi Uplink Latency",portal.ParameterType.LATENCY,0,
    longDescription="A specific latency to use for the wifi uplink.")
pc.defineParameter(
    "mobileLinkSpeed","Mobile Uplink Speed",portal.ParameterType.INTEGER,1000000,
    [(0,"Any"),(100000,"100Mb/s"),(1000000,"1Gb/s"),(10000000,"10Gb/s")],
    longDescription="A specific link speed to use for the mobile uplink.  This value defaults to 1Gbps.")
pc.defineParameter(
    "mobileLinkLatency","Mobile Uplink Latency",portal.ParameterType.LATENCY,0,
    longDescription="A specific latency to use for the mobile uplink.")

params = pc.bindParameters()

if params.numAggregates < 0:
    pc.reportError(portal.ParameterError(
        "Must specify 0 or more aggregates",['numAggregates']))
if params.aggregatesPerHost < 1:
    pc.reportError(portal.ParameterError(
        "Must specify 1 or more aggregates per host",['numAggregates']))
if params.coresPerVM < 0:
    pc.reportError(portal.ParameterError(
        "Must specify at least one core per VM",['coresPerVM']))
if params.ramPerVM < 0:
    pc.reportError(portal.ParameterError(
        "Must specify at least one core per VM",['ramPerVM']))
if params.aggregateSubnetBitSize < 20 or params.aggregateSubnetBitSize > 29:
    pc.reportError(portal.ParameterError(
        "Aggregate subnet bit size must be between 20 and 29",
        ['aggregateSubnetBitSize']))

clientvars.addVariable("VPNTYPE",params.vpnType)
clientvars.addVariable("VPN_NETWORK","192.168.248.0")
clientvars.addVariable("VPN_MASK","255.255.248.0")
clientvars.addVariable("VPN_BITS",21)
clientvars.addVariable("VPN_COMPRESSION",params.vpnCompression)
clientvars.addVariable("VPN_CIPHER",params.vpnCipher)

if params.vpnType == TOPO_NET30MP:
    ipaVPN = IPAssign(baseaddr=[192,168,248,8],basebits=21,allowed_subnets=[29])
    for i in range(1,params.numAggregates+1):
        ipaVPN.request_network("agg%d" % (i,),1)
    ipaVPN.assign_networks()
    for i in range(1,params.numAggregates+1):
        name = "agg%d" % (i,)
        clientvars.addVariable(name + "_VPN_MASK",ipaVPN.get_network_mask(name))
        clientvars.addVariable(name + "_VPN_CIDR",ipaVPN.get_network_cidr(name))
        clientvars.addVariable(name + "_VPN_BITS",ipaVPN.get_network_bits(name))
        clientvars.addVariable(name + "_VPN_NETWORK",ipaVPN.get_network(name))
    pass

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

tour = ig.Tour()
tour.Description(
    ig.Tour.TEXT,"Create an emulation of the POWDER aggregate VPN backhauls.")
request.addTour(tour)

#
# Setup the IPAssign objects we'll need.  Note that we will use these in
# careful order, since they generate linearly.
#
ipaPublic = IPAssign(baseaddr=[192,168,0,0],basebits=16,
                     allowed_subnets=[24,params.aggregateSubnetBitSize,30])
ipaPrivate = IPAssign(baseaddr=[10,0,0,0],basebits=8,
                      allowed_subnets=[16,24])

# Save off our node objects.
agghosts = {}
nodes = {}
aggToVhostMap = {}
vhostToAggMap = {}
publinks = []
privlinks = []
aggNodes = {}

disableTestbedRootKeys = True
TBCMD = "sudo mkdir -p /local/setup && sudo -H /local/repository/bin/setup-driver.sh 2>&1 | sudo tee /local/setup/setup-driver.log"

def applyGenericVirtNodeSettings(node):
    node.exclusive = True
    node.addService(pg.Execute(shell="sh",command=TBCMD))
    if disableTestbedRootKeys:
        node.installRootKeys(False, False)
    if params.hostType:
        node.hardware_type = params.hostType
    if params.hostImage:
        node.disk_image = params.hostImage

def applyGenericPhysNodeSettings(node):
    node.exclusive = True
    node.addService(pg.Execute(shell="sh",command=TBCMD))
    if disableTestbedRootKeys:
        node.installRootKeys(False, False)
    if params.hostType:
        node.hardware_type = params.hostType
    if params.vmImage:
        node.disk_image = params.vmImage

def applyGenericVnodeSettings(node):
    node.exclusive = True
    node.addService(pg.Execute(shell="sh",command=TBCMD))
    if disableTestbedRootKeys:
        node.installRootKeys(False, False)
    if params.vmImage:
        node.disk_image = params.vmImage
    if params.coresPerVM > 0:
        node.cores = params.coresPerVM
    if params.ramPerVM:
        node.ram = params.ramPerVM

if params.virtualizeAllFixedNodes:
    virtFixedNodes = ["fw","mboss","mops","con","inet","wifi","mobile"]
    physFixedNodes = []
else:
    virtFixedNodes = ["mops","inet"]
    physFixedNodes = ["fw","mboss","con","wifi","mobile"]

#
# First, create the fixed infrastructure.
#
for name in physFixedNodes:
    node = pg.RawPC(name)
    applyGenericPhysNodeSettings(node)
    nodes[node.client_id] = node
    pass
fixedVhost = pg.RawPC("fixedhost")
applyGenericVirtNodeSettings(fixedVhost)
for name in virtFixedNodes:
    node = ig.XenVM(name)
    applyGenericVnodeSettings(node)
    node.InstantiateOn(fixedVhost.client_id)
    nodes[node.client_id] = node
    clientvars.addVariable("%s_HOST" % (name),fixedVhost.client_id)
    pass
clientvars.addVariable("CONCENTRATOR","con")
clientvars.addVariable("MBOSS","mboss")
clientvars.addVariable("FW","fw")

#
# Second, create the aggregate vhosts and aggregates.
#
vcount = 0
vhost = None
for i in range(1,params.numAggregates+1):
    if (i - 1) % params.aggregatesPerHost == 0:
        vcount += 1
        vhost = pg.RawPC("ctlnuc%d" % (vcount))
        applyGenericVirtNodeSettings(vhost)
        agghosts[vhost.client_id] = vhost
        vhostToAggMap[vhost.client_id] = []
    aggName = "agg%d" % (i)
    aggToVhostMap[aggName] = vhost
    clientvars.addVariable("%s_HOST" % (aggName),vhost.client_id)
    #
    # Now create the aggregate: ctl, boss (nuc1,2,3)
    #
    nodelist = ["boss","nuc1"]
    if params.doAllNucs:
        nodelist.extend(["nuc2","nuc3"])
    aggNodes[aggName] = []
    vhostToAggMap[vhost.client_id].append(aggName)
    nodelist = map(lambda x: "%s%s" % (aggName,x),nodelist)
    for name in nodelist:
        node = ig.XenVM(name)
        applyGenericVnodeSettings(node)
        node.InstantiateOn(vhost.client_id)
        nodes[node.client_id] = node
        aggNodes[aggName].append(node)
        clientvars.addVariable("%s_HOST" % (name),vhost.client_id)
    pass
for vhostname in sorted(agghosts.keys()):
    clientvars.addVariable(
        "%s_AGGREGATES" % (vhostname)," ".join(sorted(vhostToAggMap[vhostname])))
clientvars.addVariable("AGGHOSTS"," ".join(sorted(agghosts.keys())))
clientvars.addVariable("AGGREGATES"," ".join(sorted(aggNodes.keys())))


def applyGenericNetworkSettings(lanlink,bandwidth=0,latency=0):
    if bandwidth > 0 or latency > 0:
        if bandwidth > 0:
            lanlink.bandwidth = bandwidth
        if latency > 0:
            lanlink.latency = latency
    else:
        if params.bestEffort is True:
            lanlink.best_effort = True
        else:
            lanlink.best_effort = False
        if params.trivialOk is True:
            lanlink.trivial_ok = True
        else:
            lanlink.trivial_ok = False
    if params.multiplex is True:
        lanlink.link_multiplexing = True
    else:
        lanlink.link_multiplexing = False
    return

ifaceCounts = {}
def nextIfaceNum(node):
    if not node in ifaceCounts:
        ifaceCounts[node] = 0
    ifaceCounts[node] += 1
    return ifaceCounts[node]

#
# Create the non-aggregate networks.
#
if params.fixedLinkSpeed == 0:
    if params.virtualizeAllFixedNodes:
        params.fixedLinkSpeed = 1000000
    else:
        params.fixedLinkSpeed = 10000000
for (mlist,name,shaping) in [
        (["fw","con"],"conlink",
         [params.fixedLinkSpeed,0]),
        (["fw","mboss","mops"],"mlan",
         [params.fixedLinkSpeed,0]),
        (["fw","mobile"],"mobileuplink",
         [params.mobileLinkSpeed,params.mobileLinkLatency]),
        (["fw","wifi"],"wifiuplink",
         [params.wifiLinkSpeed,params.wifiLinkLatency]),
        (["fw","inet"],"inetuplink",[]) ]:
    if len(mlist) == 2:
        if not name:
            name = "%s-%s-link" % (mlist[0],mlist[1])
        lanlink = pg.Link(name)
    else:
        if not name:
            name = "%s-lan" % (mlist[0])
        lanlink = pg.LAN(name)
    applyGenericNetworkSettings(lanlink,*shaping)
    ifaces = map(lambda x: nodes[x].addInterface("if%d" % (nextIfaceNum(x))),
                 mlist)
    map(lambda x: lanlink.addInterface(x),ifaces)
    publinks.append(lanlink)
    ipaPublic.request_network(lanlink.client_id,254)
    pass

#
# Create the aggregate networks.
#
for aggName in sorted(aggNodes.keys()):
    name = aggName + "lan"
    lanlink = pg.LAN(name)
    applyGenericNetworkSettings(lanlink)
    mlist = map(lambda x: x.client_id,aggNodes[aggName])
    ifaces = map(lambda x: nodes[x].addInterface("if%d" % (nextIfaceNum(x))),
                 mlist)
    map(lambda x: lanlink.addInterface(x),ifaces)
    publinks.append(lanlink)
    # Note we ask for len(members) + 1; + 1 for the gateway, which is a
    # VPN endpoint on the aggregate control nuc.
    ipaPublic.request_network(lanlink.client_id,len(aggNodes[aggName]) + 1)
    pass

#
# Create the aggregate uplink networks (to both mobile and wifi).  NB:
# these links are from the wifi and mobile routers to the ctlnuc vhosts
# that host each aggregate.
#
for (uplink,shaping) in [("wifi",[params.wifiLinkSpeed,params.wifiLinkLatency]),
                         ("mobile",[params.mobileLinkSpeed,params.mobileLinkLatency])]:
    upnode = nodes[uplink]
    upname = upnode.client_id
    if params.aggUplinksAreLANs:
        lanlink = pg.LAN("%slanlink" % (uplink))
        uiface = upnode.addInterface("if%d" % (nextIfaceNum(upname)))
        lanlink.addInterface(uiface)
        for (vhostname,vhost) in agghosts.iteritems():
            viface = vhost.addInterface("if%d" % (nextIfaceNum(vhostname)))
            lanlink.addInterface(viface)
        applyGenericNetworkSettings(lanlink,*shaping)
        privlinks.append(lanlink)
        ipaPrivate.request_network(lanlink.client_id,len(lanlink.interfaces))
    else:
        for (vhostname,vhost) in agghosts.iteritems():
            lanlink = pg.Link("%s%slanlink" % (vhostname,uplink))
            uiface = upnode.addInterface("if%d" % (nextIfaceNum(upname)))
            viface = vhost.addInterface("if%d" % (nextIfaceNum(vhostname)))
            lanlink.addInterface(uiface)
            lanlink.addInterface(viface)
            applyGenericNetworkSettings(lanlink,*shaping)
            privlinks.append(lanlink)
            ipaPrivate.request_network(lanlink.client_id,2)
    pass

#
# Assign all the addresses.  NB: we save an address at the bottom of
# each aggregate network, to serve as the VPN clientside address.
#
ipaPublic.assign_networks()
for lanlink in publinks:
    name = lanlink.client_id
    if name.startswith("agg"):
        (a,n) = ipaPublic.assign_host(name)
        idx = name.find("lan")
        if idx <= 0:
            raise Exception("unexpected aggregate lan name '%s'" % (name))
        vname = name[0:idx]#.upper()
        clientvars.addVariable(vname + "_GW",a)
    for iface in lanlink.interfaces:
        (a,n) = ipaPublic.assign_host(lanlink.client_id)
        iface.addAddress(pg.IPv4Address(a,n))
        idx = iface.client_id.find(":")
        if idx > 0:
            vnodename = iface.client_id[:idx]
            clientvars.addVariable("%s_%s_IP" % (vnodename,name),a)
    clientvars.addVariable(name + "_MASK",ipaPublic.get_network_mask(name))
    clientvars.addVariable(name + "_CIDR",ipaPublic.get_network_cidr(name))
    clientvars.addVariable(name + "_BITS",ipaPublic.get_network_bits(name))
    clientvars.addVariable(name + "_NETWORK",ipaPublic.get_network(name))
ipaPrivate.assign_networks()
for lanlink in privlinks:
    name = lanlink.client_id
    for iface in lanlink.interfaces:
        (a,n) = ipaPrivate.assign_host(lanlink.client_id)
        iface.addAddress(pg.IPv4Address(a,n))
        idx = iface.client_id.find(":")
        if idx > 0:
            vnodename = iface.client_id[:idx]
            clientvars.addVariable("%s_%s_IP" % (vnodename,name),a)
    clientvars.addVariable(name + "_MASK",ipaPrivate.get_network_mask(name))
    clientvars.addVariable(name + "_CIDR",ipaPrivate.get_network_cidr(name))
    clientvars.addVariable(name + "_BITS",ipaPrivate.get_network_bits(name))
    clientvars.addVariable(name + "_NETWORK",ipaPrivate.get_network(name))

#
# Add all the resources.
#
request.addResource(fixedVhost)
for x in sorted(agghosts.keys()):
    request.addResource(agghosts[x])
for x in sorted(nodes.keys()):
    request.addResource(nodes[x])
#for x in lans:
#    request.addResource(x)
for x in publinks:
    request.addResource(x)
for x in privlinks:
    request.addResource(x)
request.addResource(clientvars)

pc.printRequestRSpec(request)
