#!/bin/sh

set -x

DIRNAME=`dirname $0`

# Gotta know the rules!
if [ `id -u` -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "$DIRNAME/setup-lib.sh"

# Don't run setup-driver.sh twice
if [ -f $OURDIR/setup-driver-done ]; then
    echo "setup-driver already ran; not running again"
    exit 0
fi
logtstart "driver"

# Hack for Emulab
grep -q 155.98 /var/emulab/boot/bossip
if [ $? -eq 0 ]; then
    controlif=`cat /var/emulab/boot/controlif`
    ip route add 155.98.36.0/22 dev $controlif
    ip route add 172.16.0.0/12 dev $controlif
fi

echo "*** Setting up root ssh pubkey access across all nodes..."

# All nodes need to publish public keys, and acquire others'
mkdir -p $OURDIR
$DIRNAME/setup-root-ssh.sh 1> $OURDIR/setup-root-ssh.log 2>&1

if [ -f $SETTINGS ]; then
    . $SETTINGS
fi

if [ "$HOSTNAME" = "$CONCENTRATOR" ]; then
    echo "*** Waiting for ssh access to all nodes..."

    for node in $NODES ; do
	[ "$node" = "$CONCENTRATOR" ] && continue

	SUCCESS=1
	fqdn=`getfqdn $node`
	while [ $SUCCESS -ne 0 ] ; do
	    sleep 1
	    ssh -o ConnectTimeout=1 -o PasswordAuthentication=No \
		-o NumberOfPasswordPrompts=0 -o StrictHostKeyChecking=No \
		$fqdn /bin/ls > /dev/null
	    SUCCESS=$?
	done
	echo "*** $node is up!"
    done

    echo "*** Setting up the VPNs"
    $DIRNAME/setup-vpn.sh 1> $OURDIR/setup-vpn.log 2>&1

    # Give the VPN a chance to settle down
    #PINGED=0
    #while [ $PINGED -eq 0 ]; do
    #	sleep 2
    #	ping -c 1 $CONTROLLER
    #	if [ $? -eq 0 ]; then
    #	    PINGED=1
    #	fi
    #done
fi

#
# If this is an aggregate boss/nuc/etc, add a route via the /29 gateway
# for that aggregate.  Won't have to do this in the real thing.
#
aggprefix=`echo $HOSTNAME | sed -nre 's/^(agg[0-9]*)[a-zA-Z].*$/\1/p'`
if [ -n $aggprefix ] ; then
    eval "varname=${aggprefix}_GW"
    eval "agg_gw=\$$varname"
    ip route add 192.168.0.0/16 via $agg_gw
fi

#
# The firewall node also needs routes to the VPN networks via the
# concentrator.
#
if [ $HOSTNAME = "fw" ]; then
    for aggprefix in $AGGREGATES ; do
	agglan="${aggprefix}lan"
	eval "varname=${agglan}_CIDR"
	eval "agg_cidr=\$$varname"

	ip route add $agg_cidr via $con_conlink_IP
    done
fi

# Mark things as done right here, it's safe.
touch $OURDIR/setup-driver-done

logtend "driver"
exit 0
