#!/bin/sh

##
## Setup an OpenVPN client for the management network.  Just saves
## a couple SSH connections from the server driving the setup.
##

set -x

# Gotta know the rules!
if [ `id -u` -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/setup-lib.sh"

logtstart "vpn-client"

maybe_install_packages openvpn

#
# For each aggregate hosted on this ctlnuc host, setup a VPN, using the
# keys/crts/conf files pushed from the server.  They are already in
# $OURDIR; see setup-vpn.sh .
#

eval "varname=${HOSTNAME}_AGGREGATES"
eval "AGGREGATES=\$$varname"

for aggname in $AGGREGATES ; do
    node="${HOSTNAME}${aggname}"
    cp -pv $OURDIR/${node}* /etc/openvpn/
    cp -pv $OURDIR/ca.crt /etc/openvpn

    #
    # Get the server up
    #
    systemctl enable openvpn@${node}.service
    systemctl restart openvpn@${node}.service
done

logtend "vpn-client"

exit 0
